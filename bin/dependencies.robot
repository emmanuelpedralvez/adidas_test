*** Settings ***
#STANDARD LIBRARIES
Library    Dialogs
Library    BuiltIn
Library    Collections
Library    OperatingSystem
Library    Process
Library    String

#EXTERNAL LIBRARIES
Library    Selenium2Library
Library    HttpLibrary.HTTP
#Library    AutoItLibrary
Library    RequestsLibrary

#CUSTOM LIBRARIES
Library    com.tinkr.web.DriverHelper

#CORE RESOURCES
Resource    ../../core-engine/bin/core.robot
Resource    ../../core-engine/bin/plugins.robot

#PROJECT SPECIFIC RESOURCES
#Resource    ../src/main/robot/ced/keywords/adapter.robot
#Resource    ../src/main/robot/lifelines/keywords/adapter.robot