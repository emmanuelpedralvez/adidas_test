*** Settings ***
Resource    ../../../../bin/dependencies.robot
Resource    ../../../../bin/environment.robot
Force Tags  adidas

Test Setup  Create Chrome Browser   ${URL_ADIDAS_HOMEPAGE}
Test Teardown   Close All Browsers

*** Variables ***
${URL_ADIDAS_HOMEPAGE}  http://store.demoqa.com
${AUTHOR_NAME}  Emmanuel Pedralvez
${INVALID_EMAIL}    invalidemail
${VALID_EMAIL}  sample@yopmail.com



*** Test Cases ***
Add Comment
    ${RANDOM}   Generate Random String

    Given user goes to sample page
    When the form sent with invalid email   ${RANDOM}   ${AUTHOR_NAME}  ${INVALID_EMAIL}
    And validated that error message is displayed
    And revised the email address to be valid   ${VALID_EMAIL}
    Then page should display the comment    ${RANDOM}

*** Keywords ***
User Goes To Sample Page
    Click Link  ${TEXT_SAMPLE_PAGE}

the form sent with invalid email
    [Arguments]     ${COMMENT}  ${AUTHOR}   ${EMAIL}
    Input Text  ${ID_COMMENT_TEXTBOX}   ${COMMENT}
    Input Text  ${ID_AUTHOR}    ${AUTHOR}
    Input Text  ${ID_EMAIL}  ${EMAIL}
    Click   ${ID_SUBMIT_BUTTON}

Validated that error message is displayed
    Page Should Contain    ${TEXT_ERROR_PAGE_BODY}

Revised the email address to be valid
    [Arguments]     ${EMAIL}
    Click Link  ${LINK_BACK}
    Input Text  ${ID_EMAIL}  ${EMAIL}
    Click   ${ID_SUBMIT_BUTTON}

Page should display the comment
    [Arguments]     ${COMMENT}
    Element Should Contain    ${ID_COMMENT_SECTION}     ${COMMENT}