import Runner
import com.tinkr.Rearm
import org.gradle.api.Project
import org.gradle.api.tasks.TaskExecutionException
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Assert
import org.junit.Test

class TestRunner {
    @Test
    public void robot() {
        Properties properties = new Properties()
        File propertiesFile = new File('robot.properties')
        Rearm robot = new Rearm()
        propertiesFile.withInputStream { properties.load(it) }
        Project project = ProjectBuilder.builder().withProjectDir(new File("").absoluteFile).build()
        project.pluginManager.apply 'org.roboscratch.robot'
        def resources = []
        def kill = "taskkill /f /im chromedriver.exe"
        def task = project.task("robot", type: Runner) {
            resources.add("PROJECT_ROOT:" + robot.getProjectRoot())
            resources.add("PROJECT_MAIN_RESOURCES:" + robot.getProjectMainResources())
            resources.add("PROJECT_TEST_RESOURCES:" + robot.getProjectTestResources())
            resources.add("CORE_ROOT:" + robot.getCoreRoot())
            resources.add("CORE_MAIN_RESOURCES:" + robot.getCoreMainResources())
            resources.add("CORE_WEBDRIVER:" + robot.getCoreWebdriver())
            variables = resources
            pythonpath = robot.getPythonPath()
            bgreport = robot.getReportBg()
            console = robot.getLogMode()
            exclude = properties.IGNORE
            include = properties.TEST
            outputdir = properties.TARGET
            data_sources = properties.SOURCE
        }
        try {
            robot.setLibraryCache()
            task.execute()
            kill.execute()
        } catch (TaskExecutionException e) {
            Assert.fail("Test Failed")
        }
    }
}