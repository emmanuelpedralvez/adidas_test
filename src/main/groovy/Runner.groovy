import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.TaskAction
import org.robotframework.RobotFramework

class Runner extends DefaultTask {
    def outputdir = null
    def data_sources = null
    def pythonpath = null
    def console = null
    def bgreport = null
    def variablefiles = null
    def variables = null
    def ignoreFailures = false
    def suites = null
    def suitename = null
    def suitedoc = null
    def metadata = null
    def tags = null
    def tests = null
    def rerunfailed = null
    def include = null
    def exclude = null
    def debugfile = null
    def outputpath = null
    def log = null
    def rc = -1

    def parseArrayArg(arguments, argToParse, optStr) {
        if (argToParse != null) {
            if (argToParse instanceof String) {
                arguments += optStr
                arguments += argToParse
            } else {
                argToParse.each {
                    arguments += optStr
                    arguments += it
                }
            }
        }
        return arguments
    }

    def parseFileArrayArg(arguments, argToParse, optStr) {
        if (argToParse != null) {
            if (argToParse instanceof String) {
                arguments += optStr
                arguments += project.file(argToParse).absolutePath
            } else {
                argToParse.each {
                    arguments += optStr
                    arguments += project.file(it).absolutePath
                }
            }
        }
        return arguments
    }

    def parseSingleArg(arguments, argToParse, optStr) {
        if (argToParse != null) {
            arguments += optStr
            arguments += argToParse
        }
        return arguments
    }

    @TaskAction
    def run() {
        def arguments = ["-d", project.file(outputdir).absolutePath]
        arguments = parseSingleArg(arguments, pythonpath, "-P")
        arguments = parseSingleArg(arguments, console, "-C")
        arguments = parseSingleArg(arguments, bgreport, "--reportbackground")
        arguments = parseSingleArg(arguments, "TAG:flatten", "--flattenkeywords")
        arguments = parseSingleArg(arguments, "TAG:remove", "--removekeywords")
        arguments = parseFileArrayArg(arguments, variablefiles, "-V")
        arguments = parseArrayArg(arguments, variables, "-v")
        arguments = parseArrayArg(arguments, suites, "-s")
        arguments = parseSingleArg(arguments, suitename, "-N")
        arguments = parseSingleArg(arguments, suitedoc, "-D")
        arguments = parseArrayArg(arguments, metadata, "-M")
        arguments = parseArrayArg(arguments, tags, "-G")
        arguments = parseArrayArg(arguments, tests, "-t")
        arguments = parseSingleArg(arguments, rerunfailed, "-R")
        arguments = parseArrayArg(arguments, include, "-i")
        arguments = parseArrayArg(arguments, exclude, "-e")
        arguments = parseSingleArg(arguments, debugfile, "-b")
        arguments = parseSingleArg(arguments, outputpath, "-o")
        arguments = parseSingleArg(arguments, log, "-l")

        FileCollection sourcesAsFile = project.files(data_sources)
        sourcesAsFile.each { File file -> arguments += file.absolutePath }

        //This will bring up Log messages in the report html
        Thread.currentThread().setName("MainThread");

        rc = RobotFramework.run((String[]) arguments)
        if (!ignoreFailures && rc != 0) {
            throw new GradleException("Test failed with return code " + rc);
        }
    }
}
